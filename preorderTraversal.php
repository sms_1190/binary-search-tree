<?php
include_once("node.php");
/*
File Name : InorderTraversal.php
Description : This is class file for printing binary tree in postorder and return sorted array.
Version : 1.0
Author : Mohit Shah
*/

class PreorderTraversal{
	
	public function __construct(){
	}
	
	//method for printing binary tree preorder
	public function printPreorder($root){
		if($root==NULL){
			return;
		}
		else{
			//printing data of root node
			echo $root->getData()." ";
			
			//printing left subtree
			if($root->getLeft()!=NULL)
			$this->printPreOrder($root->getLeft());
			
			//printing right subtree
			if($root->getRight()!=NULL)
			$this->printPreOrder($root->getRight());
		}
	}
}
?>
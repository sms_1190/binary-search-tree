<?php
include_once("node.php");
/*
File Name : binaryTreeHeight.php
Description : This is class file for finding height of Binary Search Tree. 
Version : 1.0
Author : Mohit Shah
*/

class BinaryTreeHeight{
	
	public function __construct(){
		
	}
	
	/**
	Function for finding the height of Tree
	*/
	public function findHeight($root){
		if($root==NULL){
			return 0;
		}
		else{
			//taking max height of left or right tree + 1
			return max($this->findHeight($root->getLeft()),$this->findHeight($root->getRight()))+1;
		}
	}
}
?>
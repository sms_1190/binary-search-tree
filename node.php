<?php

/*
File Name : Node.php
Description : This is file for providing prototype of Node of binary tree.
Version : 1.0
Author : Mohit Shah
*/

class Node{
	//variable for left child
	private $left;
	//variable for right child
	private $right;
	//variable for node data
	private $data;
	
	//constructor of class which initialize the data and left and right childrens
	public function __construct($d){
		$this->data=$d;
		$this->left=NULL;
		$this->right=NULL;
	}
	
	//fucntion for getting data of node
	public function getData(){
		return $this->data;
	}
	
	//function for setting data of node
	public function setData($d){
		$this->data=$d;
	}
	
	//function for getting left child of node
	public function getLeft(){
		return $this->left;
	}
	
	//function for setting left child of node
	public function setLeft($l){
		$this->left=$l;
	}
	
	//function for getting right child of node
	public function getRight(){
		return $this->right;
	}
	
	//function for setting right child of node
	public function setRight($r){
		$this->right=$r;
	}
}
?>
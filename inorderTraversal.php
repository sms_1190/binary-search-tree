<?php
include_once("node.php");
/*
File Name : InorderTraversal.php
Description : This is class file for printing binary tree in inorder and return sorted array.
Version : 1.0
Author : Mohit Shah
*/

class InorderTraversal{
	
	public function __construct(){
	}
	
	//method for printing binary tree inorder
	public function printInorder($root){
		if($root==NULL){
			return;
		}
		else{
			//printing left subtree
			if($root->getLeft()!=NULL)
			$this->printInOrder($root->getLeft());
			
			//printing data of root node
			echo $root->getData()." ";
			
			//printing right subtree
			if($root->getRight()!=NULL)
			$this->printInOrder($root->getRight());
		}
	}
}
?>
<?php
/*
File Name : Test.php
Description : This is file for testing various functionality of binary search tree
Version : 1.0
Author : Mohit Shah
*/
include_once("binarySearchTree.php");

//creating object of Binary Search Tree
$bst=new BinarySearchTree();

//insearting data
$bst->insert(10);
$bst->insert(20);
$bst->insert(5);
$bst->insert(30);

//printing BinarySearchTree Inorder
echo "<h5>Inorder Traversal of Binary Tree</h5>";

$bst->printInOrder();

//printing BinarySearchTree Postorder
echo "<h5>Postorder Traversal of Binary Tree</h5>";
$bst->printPostOrder();

//printing BinarySearchTree Preorder
echo "<h5>Preorder Traversal of Binary Tree</h5>";
$bst->printPreOrder();

//getting height of Binary Tree
echo "<h5>Height of Binary Tree</h5>";
echo $bst->getHeight();

//getting toatl children of Binary Tree
echo "<h5>Total Children of Binary Tree</h5>";
echo $bst->getTotalChildren();

//getting toatl nodes of Binary Tree
echo "<h5>Total Nodes of Binary Tree</h5>";
echo $bst->getTotalNodes();

//getting toatl leavs of Binary Tree
echo "<h5>Total Leaves of Binary Tree</h5>";
echo $bst->getTotalLeaves();

?>
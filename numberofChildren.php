<?php
include_once("node.php");
/*
File Name : numberofChildren.php
Description : This is class file for finding total number of children
Version : 1.0
Author : Mohit Shah
*/

class NumberofChildren{
	
	public function __construct(){
		
	}
	
	/**
	Function for counting total number of children
	*/
	
	public function getTotalChildren($root){
		if($root==NULL){
			return 0;
		}
		else{
			//count total nodes -1
			return $this->getTotalNodes($root)-1;
		}
	}
	
	public function getTotalNodes($root){
		if($root==NULL){
			return 0;
		}
		else{
			//count left subtree children and right subtree children
			return $this->getTotalNodes($root->getLeft())+$this->getTotalNodes($root->getRight())+1;
		}
	}
	
	
}
?>
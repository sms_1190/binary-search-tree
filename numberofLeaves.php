<?php
include_once("node.php");
/*
File Name : numberofLeaves.php
Description : This is class file for finding total number of leaves
Version : 1.0
Author : Mohit Shah
*/

class NumberofLeaves{
	
	public function __construct(){
		
	}
	
	/**
	Function for counting total number of leaves
	*/
	public function getTotalLeaves($root){
		if($root==NULL){
			return 0;
		}
		//if both left and right nodes are NULL,return one
		else if($root->getLeft()==NULL && $root->getRight()==NULL){
			return 1;
		}
		else{
			//recursively get Leaves from left and right subtrees
			return $this->getTotalLeaves($root->getLeft())+$this->getTotalLeaves($root->getRight());
		}
	}
	
}
?>
<?php
include_once("node.php");
/*
File Name : InorderTraversal.php
Description : This is class file for printing binary tree in postorder and return sorted array.
Version : 1.0
Author : Mohit Shah
*/

class PostorderTraversal{
	
	public function __construct(){
	}
	
	//method for printing binary tree postorder
	public function printPostorder($root){
		if($root==NULL){
			return;
		}
		else{
			//printing left subtree
			if($root->getLeft()!=NULL)
			$this->printPostOrder($root->getLeft());
			
			//printing right subtree
			if($root->getRight()!=NULL)
			$this->printPostOrder($root->getRight());
			
			//printing data of root node
			echo $root->getData()." ";
		}
	}
}
?>
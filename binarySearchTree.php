<?php
/*
File Name : BinarySearchTree.php
Description : This is class file for Binary Search Tree. Different methods like insert node,
delete node,count height of tree etc are being implemented.
Version : 1.0
Author : Mohit Shah
*/

include_once("node.php");
include_once("inorderTraversal.php");
include_once("postorderTraversal.php");
include_once("preorderTraversal.php");
include_once("binaryTreeHeight.php");
include_once("numberofChildren.php");
include_once("numberofLeaves.php");

class BinarySearchTree{
	
	//variable for root node
	private $root;
	
	//constructor for BinarySearchTree which initialize root to NULL
	public function __construct(){
		$this->root=NULL;
	}
	
	public function insert($data){
		$this->root=$this->add($this->root,$data);
	}
	
	/**
	Function for adding nodes recursively in BST
	*/
	public function add($root,$data){
		//if root node is empty,create root node
		if($root==NULL){
			$root=new Node($data);
		}
		else{
			//if data less than or equal to root node, add recursively into left subtree
			if($data<=$root->getData()){
				$root->setLeft($this->add($root->getLeft(),$data));
			}
			//if data greater than or equal to root node, add recursively into right subtree
			else if($data>=$root->getData()){
				$root->setRight($this->add($root->getRight(),$data));
			}
		}
		return $root;
	}
	
	/**
	Function for printing Binary Tree inorder
	*/
	public function printInOrder(){
		//create object of InorderTraversal
		$inorder=new InorderTraversal();
		//calling printInorder method of Inordertraversal class
		$inorder->printInorder($this->root);
	}
	
	/**
	Function for printing Binary Tree postorder
	*/
	public function printPostOrder(){
		//create object of PostorderTraversal
		$postorder=new PostorderTraversal();
		//calling printPostorder method of Postordertraversal class
		$postorder->printPostorder($this->root);
		
	}
	
	/**
	Function for printing Binary Tree preorder
	*/
	public function printPreOrder(){
		//create object of PreorderTraversal
		$preorder=new PreorderTraversal();
		//calling printPreorder method of Preordertraversal class
		$preorder->printPreorder($this->root);
	}
	
	/**
	Function for getting height of Binary Tree
	*/
	public function getHeight(){
		//create object of BinaryTreeHeight
		$height=new BinaryTreeHeight();
		//calling findHeight method of BinaryTreeHeight class
		return $height->findHeight($this->root);
	}
	
	/**
	Function for getting total number of children
	*/
	public function getTotalChildren(){
		//create object of NumberofChildren
		$numberofChildren=new NumberofChildren();
		//calling getTotalChildren method of NumberofChildren class
		return $numberofChildren->getTotalChildren($this->root);
	}
	
	/**
	Function for getting total number of nodes
	*/
	public function getTotalNodes(){
		//create object of NumberofChildren
		$numberofChildren=new NumberofChildren();
		//calling getTotalChildren method of NumberofChildren class
		return $numberofChildren->getTotalNodes($this->root);
	}
	
	/**
	Function for getting total number of leaves
	*/
	public function getTotalLeaves(){
		//create object of NumberofLeaves
		$numberofLeaves=new NumberofLeaves();
		//calling getTotalLeaves method of NumberofChildren class
		return $numberofLeaves->getTotalLeaves($this->root);
	}
}
?>